package hello.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import hello.entity.Student;

@Mapper
public interface StudentMapper {

	Student findById(Long id);
	
	void updateSexById (@Param("id")Long id, @Param("sex")String sex);
}
