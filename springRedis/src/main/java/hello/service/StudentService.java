package hello.service;

public interface StudentService {
	
	public Object findById(Long id);
	
	public void updateSexById(Long id, String sex);

}
