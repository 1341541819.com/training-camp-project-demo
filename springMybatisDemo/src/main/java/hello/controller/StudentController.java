package hello.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import hello.service.StudentService;


@Controller
public class StudentController {
	
	@Autowired
	private StudentService studentService;
	
	@RequestMapping("/login")
	public String login() {
		System.out.println("login!");
		return "login";
	}
	
	@RequestMapping(value = "/submit", method=RequestMethod.POST)
	public String submit(String username, String password) {
		System.out.println("submit!!");
		boolean isLogin = studentService.login(username, password);
		if(isLogin) {
			return "loginSucc";
		} else {
			return "loginFail";
		}
	}
	
	
}
